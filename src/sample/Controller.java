package sample;

import java.io.IOException;
import java.sql.ResultSet;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Controller {
	static String nickName;

	@FXML
	private TextField login_field;

	@FXML
	private Button login_button;

	@FXML
	private Pane LoginPane;

	@FXML
	private PasswordField pwd_field;

	@FXML
	private Button signin_button;

	@FXML
	void initialize() {
		login_button.setOnAction(event -> {
			String login = login_field.getText().trim();
			String pwd = pwd_field.getText().trim();
			if (!login.equals("") && !pwd.equals("")) {
				try {

					DBConnector dbConnector = new DBConnector();
					User user = new User();
					user.setLogin(login_field.getText());
					user.setPwd(pwd_field.getText());
					ResultSet res = dbConnector.getUsers(user);

					if (true) {

						login_field.getScene().getWindow().hide();
						FXMLLoader loader = new FXMLLoader();
						loader.setLocation(getClass().getResource("MainWindow.fxml"));
						try {
							loader.load();
						} catch (IOException e) {
							e.printStackTrace();
						}
						Parent parent = loader.getRoot();
						Stage stage = new Stage();
						stage.setScene(new Scene(parent));
						stage.show();
						nickName = login_field.getText();

					} else {
						System.out.println("Not success");
					}
				} catch (Exception ex) {
				}
			} else {
				System.out.println("Error");
			}
		});

		signin_button.setOnAction(event -> {
			signin_button.getScene().getWindow().hide();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("reg.fxml"));
			try {
				loader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Parent parent = loader.getRoot();
			Stage stage = new Stage();
			stage.setScene(new Scene(parent));
			stage.show();
		});

	}

	@FXML
    void sendMsg(ActionEvent event) {
		login_button.getScene().getWindow().hide();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("MainWindow.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Parent parent = loader.getRoot();
		Stage stage = new Stage();
		stage.setScene(new Scene(parent));
		stage.show();
		nickName = login_field.getText();
    	
         }
	}


