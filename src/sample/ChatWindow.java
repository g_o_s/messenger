package sample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;




public class ChatWindow {


  @FXML
   private TextArea text;

    @FXML
    private Button send_button;

    @FXML
    private TextField text_field;

    private static BufferedReader bufferedReader;
    private static PrintWriter printWriter;
    private static String nickName =Controller.nickName;

    @FXML
    void initialize() throws Exception {
        nickName = Controller.nickName;
        System.out.println(nickName + " has been conected");
        connection();
        Thread thread = new Thread(new Listener());
        thread.start();


    }
    private static void connection() throws Exception {

        System.out.println("ChatWindow.connection");
        Socket socket = new Socket("127.0.0.1", 57336);
        InputStreamReader inpS = new InputStreamReader(socket.getInputStream());
        bufferedReader = new BufferedReader(inpS);
        printWriter = new PrintWriter(socket.getOutputStream());

    }

    public void sendMsg(ActionEvent actionEvent) {
        String msg = nickName + ": " + text_field.getText();
        printWriter.println(msg);
        printWriter.flush();

        text_field.setText("");

    }

    private  class Listener implements Runnable {

        @Override
        public void run() {
            String msg;
            try {
                while ((msg = bufferedReader.readLine()) != null) {
                    text.appendText("vasa");
                }

            } catch (Exception e) {
            }

        }

    }
}

