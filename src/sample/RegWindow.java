package sample;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class RegWindow {

  

    @FXML
    private Button reg_button;

    @FXML
    private TextField login_field;

    @FXML
    private Pane LoginPane;

    @FXML
    private PasswordField cpwd_field;

    @FXML
    private PasswordField pwd_field;

    @FXML
    void initialize() throws IOException {

        DBConnector dbConnector= new DBConnector();

      reg_button.setOnAction(event -> {
          if(pwd_field.getText().equals(cpwd_field.getText())) {
              try {
                  dbConnector.signUpUser(login_field.getText(), pwd_field.getText());
              } catch (SQLException e) {
                  e.printStackTrace();
              } catch (ClassNotFoundException e) {
                  e.printStackTrace();
              }
          }
          else {
              System.out.println("Error");
          }
      });

    }
}
