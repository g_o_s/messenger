package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class Server {
    private static ArrayList<PrintWriter> streams;

    public static void main(String[] args) throws IOException {
        start();

    }

    private static void start() throws IOException {

        streams = new ArrayList<PrintWriter>();
        ServerSocket ss = new ServerSocket(57336);

        while (true) {
            Socket socket = ss.accept();
            System.out.println("User has been connected IP: " + socket.getInetAddress());
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            streams.add(writer);
            Thread tr = new Thread(new Listener(socket));
            tr.start();
        }

    }

    private static void tellEveryone(String msg) {
        int x = msg.indexOf(':');
        String login = msg.substring(0, x);
        Iterator<PrintWriter> it = streams.iterator();
        while (it.hasNext()) {
            try {
                PrintWriter writer = it.next();
                writer.println(msg);
                writer.flush();

            } catch (Exception e) {
            }
        }

    }

    private static class Listener implements Runnable {
        BufferedReader bufferedReader;

        Listener(Socket socket) throws IOException {
            InputStreamReader is = new InputStreamReader(socket.getInputStream());
            bufferedReader = new BufferedReader(is);
        }

        @Override
        public void run() {
            String msg;
            try {
                while ((msg = bufferedReader.readLine()) != null) {
                    tellEveryone(msg);
                }
            } catch (Exception e) {
            }

        }

    }
}
