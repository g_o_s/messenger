package sample;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnector {
    String URL = "jdbc:mysql://localhost:3306/messenger"+
            "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    String user ="root";
    String pwd = "12345";
    String dbName = "messenger";

    Connection dbConnectiom;

    public Connection getDbConnection() throws SQLException, ClassNotFoundException {

        Class.forName("com.mysql.cj.jdbc.Driver");

        dbConnectiom = DriverManager.getConnection(URL,user,pwd);
        return dbConnectiom;
    }
    public void signUpUser(String login, String pwd) throws SQLException, ClassNotFoundException {
        String insert = "INSERT INTO users(login,password) VALUES(?,?);";
        PreparedStatement prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1,login);
        prSt.setString(2,pwd);
        prSt.executeUpdate();
    }
    public ResultSet getUser(User user) throws SQLException, ClassNotFoundException {
        ResultSet res = null;
        String select = "SELECT * FROM users WHERE login =? AND password =?;";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1,user.getLogin());
        prSt.setString(2,user.getPwd());	
        res =  prSt.executeQuery();
        return res;
    }
    public ResultSet getUsers(User user) throws SQLException, ClassNotFoundException {
        ResultSet res = null;
        String select = "SELECT * FROM users;";
        PreparedStatement prSt = getDbConnection().prepareStatement(select);
        prSt.setString(1,user.getLogin());
        prSt.setString(2,user.getPwd());	
        res =  prSt.executeQuery();
        return res;
    }
}
